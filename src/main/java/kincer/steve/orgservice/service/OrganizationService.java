package kincer.steve.orgservice.service;

import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.repository.OrganizationRepository;
import kincer.steve.orgservice.transfer.OrganizationCreate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrganizationService {

    private static final Logger LOG = LoggerFactory.getLogger(OrganizationService.class);

    private final OrganizationRepository organizationRepository;

    public OrganizationService(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    public List<OrganizationEntity> getAll() {
        return organizationRepository.findAll();
    }

    public Optional<OrganizationEntity> findById(Integer id) {
        return organizationRepository.findById(id);
    }


    public OrganizationEntity save(OrganizationEntity organization) {
        try {
            return organizationRepository.save(organization);
        } catch (DataAccessException e) {
            LOG.warn(e.getMessage());
            return null;
        }
    }

    public boolean delete(Integer id) {
        try {
            organizationRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
