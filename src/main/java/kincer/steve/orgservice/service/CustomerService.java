package kincer.steve.orgservice.service;

import kincer.steve.orgservice.domain.CustomerEntity;
import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.repository.CustomerRepository;
import kincer.steve.orgservice.repository.OrganizationRepository;
import kincer.steve.orgservice.transfer.CustomerCreate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private static final Logger LOG = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository customerRepository;
    private final OrganizationRepository organizationRepository;

    public CustomerService(CustomerRepository customerRepository, OrganizationRepository organizationRepository) {
        this.customerRepository = customerRepository;
        this.organizationRepository = organizationRepository;
    }

    public List<CustomerEntity> getAll() {
        return customerRepository.findAll();
    }

    public Optional<CustomerEntity> findById(Integer id) {
        return customerRepository.findById(id);
    }

    public CustomerEntity create(CustomerCreate customerCreate) {

        Optional<OrganizationEntity> organizationEntity = organizationRepository.findById(customerCreate.getOrganizationId());

        if(organizationEntity.isEmpty()) return null;

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setOrganization(organizationEntity.get());
        customerEntity.setName(customerCreate.getName());

        try {
            return customerRepository.save(customerEntity);
        } catch (DataAccessException e) {
            LOG.warn(e.getMessage());
        }

        return null;
    }

    public CustomerEntity update(Integer currentId, CustomerCreate customerCreate) {

        Optional<OrganizationEntity> organizationEntity = organizationRepository.findById(customerCreate.getOrganizationId());

        if(organizationEntity.isEmpty()) return null;

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(currentId);
        customerEntity.setOrganization(organizationEntity.get());
        customerEntity.setName(customerCreate.getName());

        try {
            return customerRepository.save(customerEntity);
        } catch (DataAccessException e) {
            LOG.warn(e.getMessage());
        }

        return null;
    }

    public CustomerEntity update(CustomerEntity currentCustomer, CustomerCreate customerCreate) {

        Optional<OrganizationEntity> organizationEntity = organizationRepository.findById(customerCreate.getOrganizationId());

        if(organizationEntity.isEmpty()) return null;

        currentCustomer.setOrganization(organizationEntity.get());
        currentCustomer.setName(customerCreate.getName());

        try {
            return customerRepository.save(currentCustomer);
        } catch (DataAccessException e) {
            LOG.warn(e.getMessage());
        }

        return null;
    }


    public boolean delete(Integer id) {
        try {
            customerRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
