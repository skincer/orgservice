package kincer.steve.orgservice.repository;

import kincer.steve.orgservice.domain.OrganizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizationRepository extends JpaRepository<OrganizationEntity, Integer> {
}
