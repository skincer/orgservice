package kincer.steve.orgservice.controller;

import kincer.steve.orgservice.config.WebConstants;
import kincer.steve.orgservice.domain.CustomerEntity;
import kincer.steve.orgservice.service.CustomerService;
import kincer.steve.orgservice.transfer.Customer;
import kincer.steve.orgservice.transfer.CustomerCreate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(CustomerController.BASE_URL)
public class CustomerController {

    public static final String BASE_URL = WebConstants.API_BASE_MAPPING + "/customers";

    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers() {

        List<CustomerEntity> customers = customerService.getAll();

        if(customers == null || customers.size() < 1) return ResponseEntity.notFound().build();

        List<Customer> customerList = customers.stream().map(Customer::new).collect(Collectors.toList());

        return ResponseEntity.ok(customerList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable Integer id) {

        Optional<CustomerEntity> customerEntity = customerService.findById(id);

        if (customerEntity.isEmpty()) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(new Customer(customerEntity.get()));
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody CustomerCreate customerCreate) {

        CustomerEntity customerEntity = customerService.create(customerCreate);

        if (customerEntity != null && customerEntity.getId() > 0) {
            try {
                return ResponseEntity.created(new URI(BASE_URL + "/" + customerEntity.getId())).build();
            } catch (URISyntaxException e) {
                LOG.warn(e.getMessage());
            }
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable Integer id, @Valid @RequestBody CustomerCreate customerCreate) {

        Optional<CustomerEntity> customerEntity = customerService.findById(id);

        if(customerEntity.isEmpty()) return ResponseEntity.notFound().build();

        CustomerEntity updatedCustomer = customerService.update(customerEntity.get(), customerCreate);

        if(updatedCustomer == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        return ResponseEntity.ok(new Customer(updatedCustomer));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable Integer id) {

        Optional<CustomerEntity> customerEntity = customerService.findById(id);

        if (customerEntity.isEmpty()) return ResponseEntity.notFound().build();

        if (customerService.delete(customerEntity.get().getId())) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
