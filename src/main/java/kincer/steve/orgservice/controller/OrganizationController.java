package kincer.steve.orgservice.controller;

import kincer.steve.orgservice.config.WebConstants;
import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.service.OrganizationService;
import kincer.steve.orgservice.transfer.Organization;
import kincer.steve.orgservice.transfer.OrganizationCreate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(OrganizationController.BASE_URL)
public class OrganizationController {

    public static final String BASE_URL = WebConstants.API_BASE_MAPPING + "/organizations";

    private static final Logger LOG = LoggerFactory.getLogger(OrganizationController.class);

    private final OrganizationService organizationService;

    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @GetMapping
    public ResponseEntity<List<Organization>> getOrganizations() {

        List<OrganizationEntity> organizations = organizationService.getAll();

        if (organizations == null || organizations.size() < 1) return ResponseEntity.notFound().build();

        List<Organization> organizationList = organizations.stream().map(Organization::new).collect(Collectors.toList());

        return ResponseEntity.ok(organizationList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Organization> getOrganization(@PathVariable Integer id) {

        Optional<OrganizationEntity> organizationEntity = organizationService.findById(id);

        if (organizationEntity.isEmpty()) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(new Organization(organizationEntity.get()));
    }

    @PostMapping
    public ResponseEntity<Organization> createOrganization(@Valid @RequestBody OrganizationCreate organizationCreate) {

        OrganizationEntity organizationEntity = organizationService.save(new OrganizationEntity(organizationCreate.getName()));

        if (organizationEntity != null && organizationEntity.getId() > 0) {
            try {
                return ResponseEntity.created(new URI(BASE_URL + "/" + organizationEntity.getId())).build();
            } catch (URISyntaxException e) {
                LOG.warn(e.getMessage());
            }
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Organization> updateOrganization(@PathVariable Integer id, @Valid @RequestBody OrganizationCreate organizationCreate) {

        Optional<OrganizationEntity> organizationEntity = organizationService.findById(id);

        if(organizationEntity.isEmpty()) return ResponseEntity.notFound().build();

        OrganizationEntity updatedOrganization = organizationService.save(new OrganizationEntity(id, organizationCreate.getName()));

        if(updatedOrganization == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        return ResponseEntity.ok(new Organization(updatedOrganization));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Organization> deleteOrganization(@PathVariable Integer id) {

        Optional<OrganizationEntity> organizationEntity = organizationService.findById(id);

        if (organizationEntity.isEmpty()) return ResponseEntity.notFound().build();

        if (organizationService.delete(organizationEntity.get().getId())) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
