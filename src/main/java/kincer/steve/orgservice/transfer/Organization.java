package kincer.steve.orgservice.transfer;

import kincer.steve.orgservice.domain.OrganizationEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class Organization {

    private Integer id;

    @NotBlank
    private String name;

    public Organization() {
    }

    public Organization(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Organization(OrganizationEntity organizationEntity) {
        this.id = organizationEntity.getId();
        this.name = organizationEntity.getName();
    }
}
