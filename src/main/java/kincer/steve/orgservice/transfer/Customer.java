package kincer.steve.orgservice.transfer;

import kincer.steve.orgservice.domain.CustomerEntity;
import lombok.Data;

@Data
public class Customer {

    private Integer id;

    private String name;

    private Integer organizationId;

    public Customer() {
    }

    public Customer(Integer id, String name, Integer organizationId) {
        this.id = id;
        this.name = name;
        this.organizationId = organizationId;
    }

    public Customer(CustomerEntity customerEntity) {
        this.id = customerEntity.getId();
        this.name = customerEntity.getName();
        this.organizationId = (customerEntity.getOrganization() == null ? null : customerEntity.getOrganization().getId());
    }
}
