package kincer.steve.orgservice.transfer;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OrganizationCreate {

    @NotBlank
    private String name;

    public OrganizationCreate() {
    }

    public OrganizationCreate(String name) {
        this.name = name;
    }
}
