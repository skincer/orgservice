package kincer.steve.orgservice.transfer;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CustomerCreate {

    @NotBlank
    private String name;

    @NotNull
    @Min(1)
    private Integer organizationId;

    public CustomerCreate() {
    }

    public CustomerCreate(String name, Integer organizationId) {
        this.name = name;
        this.organizationId = organizationId;
    }
}
