package kincer.steve.orgservice.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class CustomerEntity {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private OrganizationEntity organization;

    public CustomerEntity() {
    }

    public CustomerEntity(Integer id, String name, OrganizationEntity organization) {
        this.id = id;
        this.name = name;
        this.organization = organization;
    }
}
