package kincer.steve.orgservice.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class OrganizationEntity {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    public OrganizationEntity() {
    }

    public OrganizationEntity(String name) {
        this.name = name;
    }

    public OrganizationEntity(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
