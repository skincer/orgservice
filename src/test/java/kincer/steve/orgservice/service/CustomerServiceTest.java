package kincer.steve.orgservice.service;

import kincer.steve.orgservice.domain.CustomerEntity;
import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.repository.CustomerRepository;
import kincer.steve.orgservice.repository.OrganizationRepository;
import kincer.steve.orgservice.transfer.CustomerCreate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private OrganizationRepository organizationRepository;

    @Test
    void getAll() {

        List<CustomerEntity> mockEntities = List.of(
                new CustomerEntity(1, "J Jones", new OrganizationEntity(1, "HR")),
                new CustomerEntity(1, "Sandy Jones", new OrganizationEntity(1, "HR"))
        );

        Mockito.doReturn(mockEntities).when(customerRepository).findAll();

        List<CustomerEntity> returnedEntities = customerService.getAll();

        assertEquals(2, returnedEntities.size());
        assertSame(mockEntities, returnedEntities);
    }

    @Test
    void findById() {

        CustomerEntity mockEntity = new CustomerEntity(
                1,
                "Papa John",
                new OrganizationEntity(1, "HR")
        );

        Mockito.doReturn(Optional.of(mockEntity)).when(customerRepository).findById(1);

        Optional<CustomerEntity> customerEntity = customerService.findById(1);

        assertTrue(customerEntity.isPresent());
        assertSame(mockEntity, customerEntity.get());
    }

    @Test
    void findByIdNotFound() {

        Mockito.doReturn(Optional.empty()).when(customerRepository).findById(1);

        Optional<CustomerEntity> customerEntity = customerService.findById(1);

        assertTrue(customerEntity.isEmpty());
    }

    @Test
    void create() {

        CustomerCreate customerCreate = new CustomerCreate(
                "Papa John",
                1
        );

        OrganizationEntity mockOrg = new OrganizationEntity(1, "HR");
        CustomerEntity mockEntity = new CustomerEntity(
                1,
                "Papa John",
                mockOrg
        );

        Mockito.doReturn(Optional.of(mockOrg)).when(organizationRepository).findById(1);
        Mockito.doReturn(mockEntity).when(customerRepository).save(Mockito.any());

        CustomerEntity customerEntity = customerService.create(customerCreate);

        assertNotNull(customerEntity);
        assertSame(mockEntity, customerEntity);

    }

    @Test
    void delete() {

        boolean result = customerService.delete(1);

        assertTrue(result);
    }

    @Test
    void deleteFail() {

        Mockito.doThrow(new DataAccessException("Something") {}).when(customerRepository).deleteById(1);

        boolean result = customerService.delete(1);

        assertFalse(result);
    }
}