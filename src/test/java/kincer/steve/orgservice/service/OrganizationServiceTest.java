package kincer.steve.orgservice.service;

import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.repository.OrganizationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class OrganizationServiceTest {

    @Autowired
    OrganizationService organizationService;

    @MockBean
    private OrganizationRepository organizationRepository;



    @Test
    void getAll() {

        organizationService = new OrganizationService(organizationRepository);

        List<OrganizationEntity> mockEntities = List.of(
                new OrganizationEntity(1, "HR"),
                new OrganizationEntity(1, "IT")
        );

        Mockito.doReturn(mockEntities).when(organizationRepository).findAll();

        List<OrganizationEntity> returnedEntities = organizationService.getAll();

        assertEquals(2, returnedEntities.size());
        assertSame(mockEntities, returnedEntities);
    }
}