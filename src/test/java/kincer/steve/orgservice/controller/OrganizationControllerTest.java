package kincer.steve.orgservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.service.OrganizationService;
import kincer.steve.orgservice.transfer.OrganizationCreate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static kincer.steve.orgservice.controller.OrganizationController.BASE_URL;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrganizationController.class)
class OrganizationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private OrganizationService organizationService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void getOrganizations() throws Exception {

        List<OrganizationEntity> organizations = List.of(
                new OrganizationEntity(1, "HR"),
                new OrganizationEntity(2, "IT")
        );

        Mockito.when(organizationService.getAll()).thenReturn(organizations);

        mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void getOrganizationsNotFound() throws Exception {

        List<OrganizationEntity> organizations = new ArrayList<>();

        Mockito.when(organizationService.getAll()).thenReturn(organizations);

        mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    void getOrganization() throws Exception {

        OrganizationEntity organizationEntity = new OrganizationEntity(1, "HR");

        Mockito.doReturn(Optional.of(organizationEntity)).when(organizationService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

    }

    @Test
    void getOrganizationNotFound() throws Exception {

        Mockito.doReturn(Optional.empty()).when(organizationService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{id}", 1))
                .andExpect(status().isNotFound());

    }

    @Test
    void createOrganization() throws Exception {

        OrganizationCreate organizationCreate = new OrganizationCreate("HR");

        OrganizationEntity organizationEntity = new OrganizationEntity(1, organizationCreate.getName());

        Mockito.doReturn(organizationEntity).when(organizationService).save(new OrganizationEntity(organizationCreate.getName()));

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated());

    }

    @Test
    void createOrganizationInvalidRequestNameNull() throws Exception {

        OrganizationCreate organizationCreate = new OrganizationCreate(null);

        OrganizationEntity organizationEntity = new OrganizationEntity(1, organizationCreate.getName());

        Mockito.doReturn(organizationEntity).when(organizationService).save(new OrganizationEntity(organizationCreate.getName()));

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void createOrganizationInvalidRequestNameEmpty() throws Exception {

        OrganizationCreate organizationCreate = new OrganizationCreate("");

        OrganizationEntity organizationEntity = new OrganizationEntity(1, organizationCreate.getName());

        Mockito.doReturn(organizationEntity).when(organizationService).save(new OrganizationEntity(organizationCreate.getName()));
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateOrganization() throws Exception {

        OrganizationEntity organizationEntity = new OrganizationEntity(1, "HR");

        Mockito.doReturn(Optional.of(organizationEntity)).when(organizationService).findById(1);

        OrganizationCreate organizationCreate = new OrganizationCreate("OPS");

        Mockito.doReturn(new OrganizationEntity(1, organizationCreate.getName()))
                .when(organizationService)
                .save(new OrganizationEntity(1, organizationCreate.getName()));

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());

    }

    @Test
    void updateOrganizationInvalidRequestNull() throws Exception {

        OrganizationCreate organizationCreate = new OrganizationCreate(null);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateOrganizationInvalidRequestEmpty() throws Exception {

        OrganizationCreate organizationCreate = new OrganizationCreate("");

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void deleteOrganization() throws Exception {

        OrganizationEntity organizationEntity = new OrganizationEntity(1, "HR");

        Mockito.doReturn(Optional.of(organizationEntity)).when(organizationService).findById(1);

        Mockito.doReturn(true).when(organizationService).delete(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

    }

    @Test
    void deleteOrganizationNotFound() throws Exception {

        Mockito.doReturn(Optional.empty()).when(organizationService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isNotFound());

    }

    @Test
    void deleteOrganizationFail() throws Exception {

        OrganizationEntity organizationEntity = new OrganizationEntity(1, "HR");

        Mockito.doReturn(Optional.of(organizationEntity)).when(organizationService).findById(1);

        Mockito.doReturn(false).when(organizationService).delete(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isInternalServerError());

    }
}