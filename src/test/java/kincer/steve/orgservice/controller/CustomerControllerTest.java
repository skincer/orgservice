package kincer.steve.orgservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kincer.steve.orgservice.domain.CustomerEntity;
import kincer.steve.orgservice.domain.OrganizationEntity;
import kincer.steve.orgservice.service.CustomerService;
import kincer.steve.orgservice.transfer.CustomerCreate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static kincer.steve.orgservice.controller.CustomerController.BASE_URL;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerController.class)
class CustomerControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @Test
    void getCustomers() throws Exception {

        List<CustomerEntity> customers = List.of(
                new CustomerEntity(1, "Billy Bob", new OrganizationEntity(1, "HR")),
                new CustomerEntity(2, "Gavin Jones", new OrganizationEntity(1, "HR"))
        );

        Mockito.when(customerService.getAll()).thenReturn(customers);

        mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    void getCustomersNotFound() throws Exception {

        List<CustomerEntity> customers = new ArrayList<>();

        Mockito.when(customerService.getAll()).thenReturn(customers);

        mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound());

    }

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void getCustomer() throws Exception {

        CustomerEntity customerEntity = new CustomerEntity(1, "Bob Jones", new OrganizationEntity(1, "HR"));

        Mockito.doReturn(Optional.of(customerEntity)).when(customerService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

    }

    @Test
    void getCustomerNotFound() throws Exception {

        Mockito.doReturn(Optional.empty()).when(customerService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{id}", 1))
                .andExpect(status().isNotFound());

    }


    @Test
    void createCustomer() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("Jon Smith", 1);

        CustomerEntity customerEntity = new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR"));

        Mockito.doReturn(customerEntity).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated());

    }

    @Test
    void createCustomerInvalidRequestOrganizationDoesNotExist() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate(null, 6);

        Mockito.doReturn(null).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void createCustomerInvalidRequestNameNull() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate(null, 1);

        CustomerEntity customerEntity = new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR"));

        Mockito.doReturn(customerEntity).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void createCustomerInvalidRequestOrganizationNull() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("Some Customer", null);

        CustomerEntity customerEntity = new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR"));

        Mockito.doReturn(customerEntity).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void createCustomerInvalidRequestNameEmpty() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("", 1);

        CustomerEntity customerEntity = new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR"));

        Mockito.doReturn(customerEntity).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void createCustomerInvalidRequestOrganizationZero() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("Some Customer", 0);

        CustomerEntity customerEntity = new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR"));

        Mockito.doReturn(customerEntity).when(customerService).create(customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCustomer() throws Exception {

        CustomerEntity customerEntity = new CustomerEntity(1, "Papa John", new OrganizationEntity(1, "HR"));

        Mockito.doReturn(Optional.of(customerEntity)).when(customerService).findById(1);

        CustomerCreate customerCreate = new CustomerCreate("Papa Bill", 1);

        Mockito.doReturn(new CustomerEntity(1, customerCreate.getName(), new OrganizationEntity(1, "HR")))
                .when(customerService)
                .update(customerEntity, customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());

    }

    @Test
    void updateCustomerInvalidRequestOrganizationDoesNotExist() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate(null, 6);

        Mockito.doReturn(null).when(customerService).update(1, customerCreate);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCustomerInvalidRequestNullName() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate(null, 1);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCustomerInvalidRequestNullOrganization() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("Some Customer", null);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCustomerInvalidRequestEmptyName() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("", 1);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCustomerInvalidRequestZeroOrganization() throws Exception {

        CustomerCreate customerCreate = new CustomerCreate("Some Customer", 0);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(BASE_URL + "/" + 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerCreate))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());

    }

    @Test
    void deleteCustomer() throws Exception {

        CustomerEntity customerEntity = new CustomerEntity(1, "Juan Olo", new OrganizationEntity(1, "HR"));

        Mockito.doReturn(Optional.of(customerEntity)).when(customerService).findById(1);

        Mockito.doReturn(true).when(customerService).delete(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

    }

    @Test
    void deleteCustomerNotFound() throws Exception {

        Mockito.doReturn(Optional.empty()).when(customerService).findById(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isNotFound());

    }

    @Test
    void deleteCustomerFail() throws Exception {

        CustomerEntity customerEntity = new CustomerEntity(1, "Juan Olo", new OrganizationEntity(1, "HR"));

        Mockito.doReturn(Optional.of(customerEntity)).when(customerService).findById(1);

        Mockito.doReturn(false).when(customerService).delete(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "/{id}", 1))
                .andExpect(status().isInternalServerError());

    }

}